#pragma once
#include <math.h>

namespace JC
{
	const int N = 100;
	const int M = 100;

	//��������� ���������� �������
	void Read(int& n, int& m, int mas[N][M]);

	//������� ������ ����� ������������� �������� �������
	int SumMaximumElment(int& n, int& m, int mas[N][M]);
	
	//������� ������ ����� ������������ �������� �������
	int SumMinimumElement(int& n, int& m, int mas[N][M]);

	//��������� ����� ���� ��� � ���� ����� � �������
	bool Comparison(int sum_min, int sum_max);

	//������� ���������� ����� ��������� �������
	int SumColumn(int& n, int& m, int mas[N][M]);

	//�������, �������� ��� ������� �������
	void SwapColumns(int& n, int& m, int& em, int(&mas)[N][M]);

	// �������������� �������� ������� �� ���������� ����� ���������
	//int ColumnOrdering(int& n, int& m, int mas[N][M]);
}