#include <iostream>
#include "SomeFunction.hpp"

namespace JC
{

	void Read(int& n, int& m, int mas[N][M])
	{
		std::cin >> n >> m;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
			{
				std::cin >> mas[i][j];
			}
	}

	int SumMaximumElment(int& n, int& m, int mas[N][M])
	{
		int sum_max = 0;
		int max = mas[0][0];
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				if (max < mas[i][j])
					max = mas[i][j];
		while (max != 0)
		{
			sum_max += max % 10;
			max /= 10;
		}
		return sum_max;
	}
	
	int SumMinimumElement(int& n, int& m, int mas[N][M])
	{
		int sum_min = 0;
		int min = mas[0][0];
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				if (min > mas[i][j])
					min = mas[i][j];
		while (min != 0)
		{
			sum_min += min % 10;
			min /= 10;
		}
		return sum_min;
	}

	bool Comparison(int sum_min, int sum_max)
	{
		if (sum_min > sum_max)
			if ((sum_min - sum_max) > 2)
				return false;
		if (sum_max > sum_min)
			if ((sum_max - sum_min) > 2)
				return false;
		return true;
	}
	int SumColumn(int& n, int& m, int mas[N][M])
	{
		int sum = 0;
		for (int i = 0; i < n; i++)
			sum += mas[i][m];
		return sum;
	}
	void SwapColumns(int& n, int& m, int& em, int (&mas)[N][M])
	{
		for (int i = 0; i < n; i++)
			std::swap(mas[i][m], mas[i][em]);
	}
	/*void ColumnOrdering(int& n, int& m, int(&mas)[N][M])
	{
		for (int i = 0; i < m - 1; i++)
			for (int j = i + 1; j < m; j++)
				if (SumColumn(n, i, mas) > SumColumn(n, j, mas))
					SwapColumns(n, i, j, mas);
	}*/
}



   
	

