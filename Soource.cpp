/*
   ������������� ������ 6
   ������ : ���� ������������� ������� {Aij}i=1...n;j=1..n , n<=100.
   ���� ����� ���� ������������ � ������������� ��������� ������� ���������� �� �����, ��� �� 2,
   ����������� ������� ������� �� ���������� ����� ���������.
*/

#include <iostream>
#include "SomeFunction.hpp"

using namespace JC;


int main()
{
	int n, m;
	int mas[N][M];
	int sum_min = 0;
	int sum_max = 0;
	Read(n, m, mas);
	SumMaximumElment(n, m, mas);
	SumMinimumElement(n, m, mas);
	if (Comparison(sum_min, sum_max) == true)
		for (int i = 0; i < m - 1; i++)
			for (int j = i + 1; j < m; j++)
				if (SumColumn(n, i, mas) > SumColumn(n, j, mas))
					SwapColumns(n, i, j, mas);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++)
			std::cout << mas[i][j] << " ";
		std::cout << std::endl;
	}
	return 0;
}